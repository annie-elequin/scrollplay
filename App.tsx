import 'node-libs-expo/globals'
import '@rn-matrix/expo/shim.js'

import { polyfillGlobal } from 'react-native/Libraries/Utilities/PolyfillFunctions'

import React, { useContext } from 'react'
import { matrix } from '@rn-matrix/expo'

polyfillGlobal('URL', () => require('whatwg-url').URL)

matrix.initAuth()

import * as eva from '@eva-design/eva'
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components'
import { EvaIconsPack } from '@ui-kitten/eva-icons'
import ThemeContextProvider, { ThemeContext } from './src/context/ThemeContext'
import AppNavigator from './src/navigation/AppNavigator'
import { NavigationContainer } from '@react-navigation/native'
import { enableScreens } from 'react-native-screens'
import { AppContextProvider } from './src/context/AppContext'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import { LogBox, View } from 'react-native'

LogBox.ignoreAllLogs(true)

enableScreens()

export default function App() {
  return (
    <ThemeContextProvider>
      <AppContent />
    </ThemeContextProvider>
  )
}

function AppContent() {
  const { theme } = useContext(ThemeContext)
  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={theme}>
        <SafeAreaProvider>
          <AppContextProvider>
            <NavigationContainer>
              <AppNavigator />
            </NavigationContainer>
          </AppContextProvider>
        </SafeAreaProvider>
      </ApplicationProvider>
    </>
  )
}
