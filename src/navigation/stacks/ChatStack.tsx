import { createStackNavigator } from '@react-navigation/stack'
import { Avatar, Text, useTheme } from '@ui-kitten/components'
import React from 'react'
import { Pressable } from 'react-native'
import { matrix } from '@rn-matrix/expo'
import { useObservableState } from 'observable-hooks'
import ChatListScreen from '../../scenes/chatList/ChatListScreen'
import ChatScreen from '../../scenes/chat/ChatScreen'
import color from 'color'
import ThemeType from '../../themes/themeType'
import Spacing from '../../styles/Spacing'

const Stack = createStackNavigator()

export default function ChatStack({ navigation }) {
  const theme: ThemeType = useTheme()
  const myUser = matrix.getMyUser()
  const avatar = useObservableState(myUser?.avatar$)
  const name: string | undefined = useObservableState(myUser?.name$)

  // const {themeId, setTheme} = useContext(ThemeContext);

  const navToSettings = () => {
    navigation.navigate('Settings')
  }

  // const toggleTheme = () => {
  //   setTheme(themeId === 'light' ? 'dark' : 'light');
  // };

  const headerTextColor = color(theme['color-primary-default']).isDark() ? '#fff' : '#000'

  return (
    <Stack.Navigator
      headerMode="screen"
      screenOptions={{
        headerStyle: { backgroundColor: theme['color-primary-default'], shadowColor: 'transparent' },
        headerTintColor: headerTextColor,
      }}
    >
      <Stack.Screen
        name="ChatList"
        options={{
          title: 'Messages',
          headerLeft: () => (
            <Pressable
              onPress={navToSettings}
              style={({ pressed }) => ({
                position: 'relative',
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: 18,
                opacity: pressed ? 0.6 : 1,
                padding: Spacing.l,
                paddingLeft: 0,
              })}
            >
              <Avatar
                source={{ uri: matrix.getHttpUrl(avatar) }}
                size="small"
                style={{
                  backgroundColor: theme['background-basic-color-3'],
                }}
              />
              {!avatar && (
                <Text style={{ position: 'absolute', opacity: 0.2 }} category="h6">
                  {name?.charAt(0)}
                </Text>
              )}
            </Pressable>
          ),
        }}
        component={ChatListScreen}
      />
      <Stack.Screen
        name="Chat"
        component={ChatScreen}
        options={({ route }) => ({
          title: route.params?.chatName || 'Chat',
          headerBackTitle: 'Back',
        })}
      />
    </Stack.Navigator>
  )
}
