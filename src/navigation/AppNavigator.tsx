import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import ChatList from '../scenes/chatList/ChatListScreen'
import { matrix } from '@rn-matrix/expo'
import { useObservableState } from 'observable-hooks'
import ThemeType from '../themes/themeType'
import { Layout, useTheme } from '@ui-kitten/components'
import color from 'color'
import { createNativeStackNavigator } from 'react-native-screens/native-stack'
import { ActivityIndicator, View } from 'react-native'
import LoginScreen from '../scenes/auth/LoginScreen'
import SettingsScreen from '../scenes/settings/SettingsScreen'
import ChatStack from './stacks/ChatStack'

const Stack = createStackNavigator()
const NativeStack = createNativeStackNavigator()

export default function AppNavigator() {
  const theme: ThemeType = useTheme()

  const authLoaded = useObservableState(matrix.authIsLoaded$())
  const authLoggedIn = useObservableState(matrix.isLoggedIn$())
  const matrixReady = useObservableState(matrix.isReady$())

  console.log({ authLoaded, authLoggedIn, matrixReady })

  const headerTextColor = color(theme['color-primary-default']).isDark() ? '#fff' : '#000'

  if (!authLoaded || (authLoggedIn && !matrixReady)) {
    return (
      <Layout level="3" style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" color={theme['color-basic-100']} />
      </Layout>
    )
  } else if (authLoggedIn) {
    return (
      <NativeStack.Navigator screenOptions={{ headerShown: false, stackPresentation: 'modal' }}>
        <NativeStack.Screen name="Chat" component={ChatStack} />
        <NativeStack.Screen name="Settings" component={SettingsScreen} />
        {/* <NativeStack.Screen name="ChatStack" component={ChatStack} />
        <NativeStack.Screen name="Lightbox" component={LightboxScreen} />
        <NativeStack.Screen name="Settings" component={SettingsStack} /> */}
      </NativeStack.Navigator>
    )
  } else {
    return (
      <NativeStack.Navigator screenOptions={{ headerShown: false }}>
        <NativeStack.Screen name="Login" component={LoginScreen} />

        {/* <NativeStack.Screen name="Landing" component={LandingScreen} />
        <NativeStack.Screen name="Login" component={LoginScreen} /> */}
      </NativeStack.Navigator>
    )
  }

  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: { backgroundColor: theme['color-primary-default'], shadowColor: 'transparent' },
        headerTintColor: headerTextColor,
      }}
    >
      <Stack.Screen name="ChatList" component={ChatList} options={{ title: 'Messages' }} />
    </Stack.Navigator>
  )
}
