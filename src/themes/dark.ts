import * as eva from '@eva-design/eva'
import { ThemeType } from '@ui-kitten/components'

const dark: ThemeType = {
  ...eva.dark
}

export default dark