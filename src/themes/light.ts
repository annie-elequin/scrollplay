import * as eva from '@eva-design/eva'
import { ThemeType } from '@ui-kitten/components'

const light: ThemeType = {
  ...eva.light
}

export default light