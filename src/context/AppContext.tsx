import React, { createContext, useEffect, useReducer } from 'react'
import { Text, View } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import Spacing from '../styles/Spacing'

const initialState = {
  toast: {
    content: null,
    isVisible: false,
  },
}

export const AppContext = createContext({})

export function AppContextProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState)

  const insets = useSafeAreaInsets()

  useEffect(() => {
    if (state.toast.isVisible) {
      // slowly hide toast
      setTimeout(() => dispatch({ type: actionTypes.HIDE_TOAST }), 3000)
    }
  }, [state.toast.isVisible])

  console.log(state.toast)

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      {children}
      {state.toast.isVisible && (
        <View
          style={{
            width: '100%',
            bottom: insets.bottom + Spacing.l,
            backgroundColor: 'red',
            position: 'absolute',
          }}
        >
          {state.toast.content}
        </View>
      )}
    </AppContext.Provider>
  )
}

const reducer = (state, action) => {
  switch (action.type) {
    case actionTypes.SHOW_TOAST:
      return { ...state, toast: { content: action.payload, isVisible: true } }
    case actionTypes.HIDE_TOAST:
      return { ...state, toast: { content: null, isVisible: false } }
    default:
      return state
  }
}

export const actionTypes = {
  SHOW_TOAST: 'SHOW_TOAST',
  HIDE_TOAST: 'HIDE_TOAST',
}
