import React, { useState } from 'react'
import light from '../themes/light'
import dark from '../themes/dark'
import ThemeType from '../themes/themeType'

export const appThemes = {
  light,
  dark,
}

export const ThemeContext = React.createContext<{ theme: ThemeType; setTheme: (theme: ThemeType) => void }>({
  theme: undefined,
  setTheme: undefined,
})

export default function ThemeContextProvider({ children }) {
  const [theme, setTheme] = useState<ThemeType>(appThemes.dark as ThemeType)
  return <ThemeContext.Provider value={{ theme, setTheme }}>{children}</ThemeContext.Provider>
}
