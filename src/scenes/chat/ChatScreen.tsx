import { Layout, List, Text, useTheme } from '@ui-kitten/components'
import React, { useEffect, useState } from 'react'
import { matrix, Message } from '@rn-matrix/expo'
import { useObservableState } from 'observable-hooks'
import { Keyboard, Platform, Pressable } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import ThemeType from '../../themes/themeType'
import { lightHaptic } from '../../utility/haptic'
import MessageItem from './components/MessageItem'
import Composer from './components/Composer'

export default function ChatScreen({ navigation, route }) {
  const chat = matrix.getRoomById(route.params?.chatId)
  const theme: ThemeType = useTheme()
  const insets = useSafeAreaInsets()

  const name = useObservableState(chat.name$)
  const messageList = useObservableState(chat.messages$)
  const typing = useObservableState(chat.typing$)
  const atStart = useObservableState(chat.atStart$)

  const [timeline, setTimeline] = useState(messageList)
  const [isLoading, setIsLoading] = useState(false)
  const [actionSheetVisible, setActionSheetVisible] = useState(false)
  const [activeMessage, setActiveMessage] = useState(null)
  const [isEditing, setIsEditing] = useState(false)
  const [isReplying, setIsReplying] = useState(false)

  const handleEndReached = async () => {
    if (!atStart && !isLoading) {
      setIsLoading(true)
      await chat.fetchPreviousMessages()
      setIsLoading(false)
    }
  }

  const renderMessageItem = ({ item, index }) => {
    return (
      <MessageItem
        key={item}
        chatId={chat.id}
        messageId={item}
        prevMessageId={messageList[index + 1] ? messageList[index + 1] : null}
        nextMessageId={messageList[index - 1] ? messageList[index - 1] : null}
        onPress={onPress}
        onLongPress={onLongPress}
      />
    )
  }

  const onPress = (message) => {
    if (Message.isImageMessage(message.type$.getValue())) {
      navigation.navigate('Lightbox', { message })
    }
  }

  const onLongPress = (message) => {
    lightHaptic()
    Keyboard.dismiss()
    // setActiveMessage(message);
    // setActionSheetVisible(true);
  }

  useEffect(() => {
    handleEndReached()
  }, [])

  useEffect(() => {
    navigation.setOptions({
      title: name,
    })
  }, [name])

  useEffect(() => {
    // mark as read
    chat.sendReadReceipt()

    // We put loading and typing indicator into the Timeline to have better
    // visual effects when we swipe to top or bottom
    if (messageList) {
      const tempTimeline = [...messageList]
      if (isLoading) tempTimeline.push('loading')
      if (typing.length > 0) tempTimeline.unshift('typing')
      setTimeline(tempTimeline)
    }
  }, [isLoading, messageList, chat, typing])

  return (
    <Layout style={{ flex: 1, paddingBottom: insets.bottom }} level="4">
      <List
        inverted
        keyboardDismissMode={Platform.OS === 'ios' ? 'interactive' : 'on-drag'}
        keyboardShouldPersistTaps="handled"
        data={timeline}
        renderItem={renderMessageItem}
        onEndReached={handleEndReached}
        style={{
          marginTop: -insets.top,
          marginHorizontal: 6,
          backgroundColor: theme['background-basic-color-4'],
        }}
      />
      <Composer chatId={chat.id} />
    </Layout>
  )
}
