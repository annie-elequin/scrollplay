import { useHeaderHeight } from '@react-navigation/stack'
import { Icon, useTheme, Button } from '@ui-kitten/components'
import React, { useRef, useState } from 'react'
import { KeyboardAvoidingView, Platform, Pressable, StatusBar, StyleSheet, TextInput, View } from 'react-native'
import Spacing from '../../../styles/Spacing'
import ThemeType from '../../../themes/themeType'
import { matrix } from '@rn-matrix/expo'

export default function Composer({ chatId }) {
  const headerHeight = useHeaderHeight()
  const theme: ThemeType = useTheme()

  const [value, setValue] = useState<string>('')
  const [isFocused, setIsFocused] = useState<boolean>(false)

  const inputRef = useRef()

  const sendDialogue = () => {
    matrix.getClient().sendMessage(chatId, {
      msgtype: 'm.dialogue',
      body: value,
    })
    setValue('')
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.select({ ios: 'padding', android: 'height' })}
      keyboardVerticalOffset={headerHeight + 2}
    >
      {/* {(isEditing || isReplying) && (
      <EditReplyView
        isReplying={isReplying}
        activeMessage={activeMessage}
        endEditingAndReplying={endEditingAndReplying}
      />
    )} */}
      <View
        style={{
          paddingHorizontal: 6,
          flexDirection: 'row',
          alignItems: 'flex-end',
          marginVertical: 6,
        }}
      >
        <TextInput
          ref={inputRef}
          style={[
            styles.input,
            {
              backgroundColor: theme['color-primary-disabled'],
              color: theme['text-basic-color'],
              borderWidth: 1.5,
              borderColor: isFocused ? theme['color-primary-default'] : 'transparent',
            },
          ]}
          multiline
          textStyle={{ paddingBottom: 6 }}
          placeholder="Message"
          placeholderTextColor={theme['text-hint-color']}
          value={value}
          onChangeText={setValue}
          onFocus={() => setIsFocused(true)}
          onBlur={() => setIsFocused(false)}
        />
        <Pressable
          disabled={value.length === 0}
          onPress={sendDialogue}
          style={({ pressed }) => ({
            backgroundColor: theme['color-primary-default'],
            padding: Spacing.xs,
            borderRadius: 80,
            opacity: pressed || value.length === 0 ? 0.5 : 1,
          })}
        >
          <Icon name="arrowhead-up" fill="#fff" width={25} height={25} />
        </Pressable>
      </View>
    </KeyboardAvoidingView>
  )
}

const styles = StyleSheet.create({
  input: {
    flex: 1,
    borderRadius: 20,
    marginRight: 6,
    paddingHorizontal: 18,
    paddingTop: 8,
    paddingBottom: 8,
    fontSize: 16,
  },
})
