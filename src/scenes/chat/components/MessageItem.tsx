import React from 'react'
import { ActivityIndicator, View } from 'react-native'
import { matrix, Message } from '@rn-matrix/expo'
import { useObservableState } from 'observable-hooks'
import { Avatar, Text, useTheme } from '@ui-kitten/components'
import Spacing from '../../../styles/Spacing'
import Dialogue from './messageTypes/Dialogue'
import ThemeType from '../../../themes/themeType'
import Event from './messageTypes/Event'

type PropsType = {
  key: string
  chatId: string
  messageId: string
  prevMessageId: string
  nextMessageId: string
  onPress: any
  onLongPress: any
}

export interface MessageProps extends PropsType {
  message: Message
  prevSame: boolean
  nextSame: boolean
  isMe: boolean
}

export default function MessageItem({
  chatId,
  messageId,
  prevMessageId,
  nextMessageId,
  onPress,
  onLongPress,
  ...otherProps
}: PropsType) {
  const myUser = matrix.getMyUser()
  const chat = matrix.getRoomById(chatId)
  const typing = useObservableState(chat?.typing$)

  if (messageId === 'typing' && typing) {
    return (
      <View style={{ marginVertical: Spacing.xs, marginLeft: Spacing.l }}>
        <Text style={{ fontStyle: 'italic' }} appearance="hint">
          {matrix.getUserById(typing[0]).name$.getValue()} {typing.length === 1 ? 'is' : 'are'} typing...
        </Text>
      </View>
    )
  }

  if (messageId === 'loading') {
    return <ActivityIndicator style={{ marginVertical: Spacing.l }} />
  }

  const message = matrix.getMessageById(messageId, chatId)
  if (!message || !message.type$) return null
  const messageType = useObservableState(message.type$)

  const prevMessage = prevMessageId && prevMessageId !== 'loading' ? matrix.getMessageById(prevMessageId, chatId) : null
  const nextMessage = nextMessageId && nextMessageId !== 'typing' ? matrix.getMessageById(nextMessageId, chatId) : null
  const prevSame = isSameSender(message, prevMessage)
  const nextSame = isSameSender(message, nextMessage)
  const isMe = myUser?.id === message.sender.id

  const onMessagePress = () => onPress(message)
  const onMessageLongPress = () => onLongPress(message)

  const props: MessageProps = {
    ...otherProps,
    onPress: onMessagePress,
    onLongPress: onMessageLongPress,
    message,
    prevSame,
    nextSame,
    nextMessageId,
    isMe,
  }

  const renderMessage = () => {
    switch (messageType) {
      case 'm.text':
      case 'm.dialogue':
        return <Dialogue {...props} />
      default:
        return null
    }
  }

  // if (message.redacted$.getValue()) {
  //   return null
  //   return <EventMessage {...props} />
  // }

  // if (Message.isTextMessage(messageType)) {
  //   return <TextMessage {...props} />
  // }
  // if (Message.isImageMessage(messageType)) {
  //   return <ImageMessage {...props} />
  // }
  // if (Message.isVideoMessage(messageType) || Message.isFileMessage(messageType)) {
  //   return <FileMessage {...props} />;
  // }
  // if (Message.isNoticeMessage(messageType)) {
  //   return <NoticeMessage {...props} />;
  // }
  // return <EventMessage {...props} />

  if (!isUserMessage(messageType)) {
    return <Event {...props} />
  }

  return <MessageWrapper {...props}>{renderMessage()}</MessageWrapper>
}

function isSameSender(messageA: Message, messageB: Message) {
  if (
    !messageA ||
    !messageB ||
    !Message.isBubbleMessage(messageA) ||
    !Message.isBubbleMessage(messageB) ||
    messageA.sender.id !== messageB.sender.id
  ) {
    return false
  }
  return true
}

function isUserMessage(type: string) {
  return type === 'm.dialogue' || type === 'm.text'
}

function MessageWrapper({ children, ...props }) {
  const { isMe, message, nextSame, nextMessageId }: MessageProps = props

  const theme: ThemeType = useTheme()
  const senderAvatar = useObservableState(message.sender.avatar$)

  return (
    <View style={{ alignItems: isMe ? 'flex-end' : 'flex-start', marginBottom: 2 }}>
      <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
        {!isMe && (
          <Avatar
            source={{ uri: matrix.getHttpUrl(senderAvatar) }}
            style={{
              backgroundColor: theme['background-basic-color-3'],
              width: 30,
              height: 30,
              opacity: nextSame ? 0 : 1,
              marginRight: Spacing.s,
            }}
          />
        )}
        {children}
      </View>
    </View>
  )
}
