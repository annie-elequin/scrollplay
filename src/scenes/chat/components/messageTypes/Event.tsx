import { Text } from '@ui-kitten/components'
import { useObservableState } from 'observable-hooks'
import React from 'react'
import { StyleSheet, View } from 'react-native'
import { MessageProps } from '../MessageItem'

export default function Event(props: MessageProps) {
  const { isMe, message, nextSame, prevSame, chat }: MessageProps = props

  const content = useObservableState(message.content$)
  const type: string | undefined = useObservableState(message.type$)

  if (!content || !showEvent(type)) return null

  return (
    <View style={styles.wrapper}>
      <Text>{content.text}</Text>
    </View>
  )
}

function showEvent(type: string | undefined) {
  return type === 'm.room.create' || type === 'm.room.member' || type === 'm.room.name'
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: 'red',
  },
})
