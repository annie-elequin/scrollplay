import { Avatar, Text, useTheme } from '@ui-kitten/components'
import { useObservableState } from 'observable-hooks'
import React from 'react'
import { StyleSheet, View } from 'react-native'
import Spacing from '../../../../styles/Spacing'
import ThemeType from '../../../../themes/themeType'
import { MessageProps } from '../MessageItem'

export default function Dialogue(props: MessageProps) {
  const { isMe, message, nextSame, prevSame, chat }: MessageProps = props

  const theme: ThemeType = useTheme()

  const content = useObservableState(message.content$)
  const senderName = useObservableState(message.sender.name$)
  const status = useObservableState(message.status$)

  if (!content) return null

  return (
    <View
      style={[
        styles.bubble,
        { backgroundColor: isMe ? theme['color-primary-default'] : theme['color-primary-disabled'] },
      ]}
    >
      <Text>{content.raw.body}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  bubble: {
    paddingVertical: Spacing.s,
    paddingHorizontal: Spacing.m,
    borderRadius: 18,
  },
})
