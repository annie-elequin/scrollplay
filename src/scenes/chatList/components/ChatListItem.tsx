import { Avatar, Icon, ListItem, Text, useTheme } from '@ui-kitten/components'
import { useObservableState } from 'observable-hooks'
import React from 'react'
import { Alert, Pressable, StyleSheet, View } from 'react-native'
import { matrix } from '@rn-matrix/expo'
import { useNavigation } from '@react-navigation/native'
import Spacing from '../../../styles/Spacing'
import ThemeType from '../../../themes/themeType'
import color from 'color'

export default function ChatListItem({ item: chat, invite = false }) {
  const theme: ThemeType = useTheme()

  const name: string | undefined = useObservableState(chat.name$)
  const snippet: string | undefined = useObservableState(chat.snippet$)
  const avatar = useObservableState(chat.avatar$)
  let unreadState = useObservableState(chat.readState$)

  const navigation = useNavigation()

  if (invite) {
    unreadState = null
  }

  const ChatAvatar = (props) => (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
      }}
    >
      <Avatar
        {...props}
        style={[
          props.style,
          {
            tintColor: null,
            width: 50,
            height: 50,
            backgroundColor: theme['background-basic-color-1'],
            borderWidth: 2,
            borderColor: unreadState === 'unread' ? theme['color-primary-default'] : 'transparent',
          },
        ]}
        source={{ uri: matrix.getHttpUrl(avatar) }}
      />
      {!avatar && (
        <Text
          style={{
            position: 'absolute',
            color: theme['text-basic-color'],
            opacity: 0.4,
          }}
          category="h5"
        >
          {name?.charAt(0)}
        </Text>
      )}
    </View>
  )

  const ChatTitle = (props) => (
    <Text
      {...props}
      style={[props.style, { fontSize: 18, fontWeight: unreadState === 'unread' || invite ? 'bold' : '300' }]}
    >
      {name}
    </Text>
  )

  const ChatDescription = (props) =>
    !invite ? (
      <Text
        {...props}
        style={[props.style, { fontSize: 14, marginTop: 3, fontWeight: unreadState === 'unread' ? 'bold' : '300' }]}
        numberOfLines={2}
      >
        {snippet?.content.trim()}
      </Text>
    ) : null

  const AcceptRejectButtons = (props) => (
    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
      <Pressable
        onPress={acceptChat}
        style={({ pressed }) => [
          styles.inviteButton,
          {
            backgroundColor: theme['text-success-color'],
            opacity: pressed ? 0.5 : 1,
          },
        ]}
      >
        <Icon name="checkmark" fill={color('#fff').alpha(0.75).hsl().toString()} width={28} height={28} />
      </Pressable>
      <Pressable
        onPress={rejectChat}
        style={({ pressed }) => [
          styles.inviteButton,
          {
            backgroundColor: theme['text-danger-color'],
            opacity: pressed ? 0.5 : 1,
          },
        ]}
      >
        <Icon name="close" fill={color('#fff').alpha(0.75).hsl().toString()} width={28} height={28} />
      </Pressable>
    </View>
  )

  const openChat = () => {
    navigation.navigate('Chat', { chatId: chat.id, chatName: chat.name$.getValue() })
  }

  const acceptChat = () => {
    matrix.joinRoom(chat.id)
    setTimeout(openChat, 200)
  }

  const rejectChat = () => {
    Alert.alert(
      'Reject Invite',
      'Are you sure? You will need another invite to join this room.',
      [
        {
          text: 'Cancel',
          onPress: undefined,
          style: 'cancel',
        },
        {
          text: 'Reject',
          onPress: () => {
            matrix.leaveRoom(chat.id)
          },
          style: 'destructive',
        },
      ],
      { cancelable: false }
    )
  }

  return (
    <ListItem
      title={ChatTitle}
      description={ChatDescription}
      accessoryLeft={ChatAvatar}
      accessoryRight={invite ? AcceptRejectButtons : undefined}
      style={{
        backgroundColor: theme[`background-basic-color-${invite ? '2' : '4'}`],
        paddingVertical: Spacing.l,
      }}
      onPress={!invite ? openChat : undefined}
      activeOpacity={invite ? 1 : 0.4}
    />
  )
}

const styles = StyleSheet.create({
  inviteButton: {
    width: 40,
    height: 40,
    borderRadius: 80,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: Spacing.m,
  },
})
