import { Text, useTheme } from '@ui-kitten/components'
import { useObservableState } from 'observable-hooks'
import React from 'react'
import { FlatList, View } from 'react-native'
import ThemeType from '../../themes/themeType'
import { matrix } from '@rn-matrix/expo'
import ChatListItem from './components/ChatListItem'

export default function ChatListScreen({ navigation }) {
  const chatList = useObservableState(matrix.getRooms$())
  const inviteList = useObservableState(matrix.getRoomsByType$('invites'))

  const theme: ThemeType = useTheme()

  const renderItem = ({ item }) => {
    return <ChatListItem item={item} />
  }

  const renderInvites = () => {
    return (inviteList || []).map((item) => <ChatListItem item={item} invite />)
  }

  return (
    <FlatList
      windowSize={10}
      maxToRenderPerBatch={6}
      data={chatList}
      renderItem={renderItem}
      style={{ backgroundColor: theme['background-basic-color-4'] }}
      ListFooterComponent={<View style={{ height: 200 }} />}
      ListHeaderComponent={renderInvites}
    />
  )
}
