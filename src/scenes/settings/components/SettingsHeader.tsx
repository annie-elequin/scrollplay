import React from 'react'
import { Avatar, Card, Text, useTheme } from '@ui-kitten/components'
import { matrix } from '@rn-matrix/expo'
import { Pressable, View, Animated } from 'react-native'
import { useObservableState } from 'observable-hooks'
import Clipboard from 'expo-clipboard'
import Spacing from '../../../styles/Spacing'
import ThemeType from '../../../themes/themeType'

export default function SettingsHeader() {
  const theme: ThemeType = useTheme()
  const myUser = matrix.getMyUser()
  const avatar: string | undefined = useObservableState(myUser?.avatar$)
  const name: string | undefined = useObservableState(myUser?.name$)

  const opacity = new Animated.Value(0)

  const copyId = () => {
    Clipboard.setString(myUser.id)
    Animated.spring(opacity, {
      toValue: 1,
      useNativeDriver: true,
    }).start()
    setTimeout(() => {
      Animated.timing(opacity, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start()
    }, 1000)
  }

  return (
    <Card
      style={{
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: Spacing.xl,
        paddingBottom: Spacing.xs,
      }}
    >
      <View style={{ position: 'relative', justifyContent: 'center', alignItems: 'center', marginBottom: Spacing.m }}>
        <Avatar
          source={{ uri: matrix.getHttpUrl(avatar) }}
          style={{
            backgroundColor: theme['background-basic-color-3'],
            width: 100,
            height: 100,
          }}
        />
        {!avatar && (
          <Text style={{ position: 'absolute', opacity: 0.2, fontSize: 50 }} category="h1">
            {name?.charAt(0)}
          </Text>
        )}
      </View>

      <Text style={{ textAlign: 'center' }} category="h5">
        {name}
      </Text>
      <Pressable
        onPress={copyId}
        style={({ pressed }) => ({ padding: Spacing.m, paddingTop: Spacing.xs, opacity: pressed ? 0.7 : 1 })}
      >
        <Text style={{ textAlign: 'center' }} appearance="hint">
          {myUser.id}
        </Text>
        <Animated.View style={{ opacity }}>
          <Text style={{ textAlign: 'center' }} appearance="hint">
            User ID copied
          </Text>
        </Animated.View>
      </Pressable>
    </Card>
  )
}
