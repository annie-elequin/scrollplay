import { Layout } from '@ui-kitten/components'
import React from 'react'
import SettingsHeader from './components/SettingsHeader'

export default function SettingsScreen() {
  return (
    <Layout style={{ flex: 1 }} level="4">
      <SettingsHeader />
    </Layout>
  )
}
