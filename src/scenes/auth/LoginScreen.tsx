import { Button, Input, Layout, Text, useTheme } from '@ui-kitten/components'
import React, { useRef, useState } from 'react'
import { ActivityIndicator, Keyboard, KeyboardAvoidingView, ScrollView, View } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import Spacing from '../../styles/Spacing'
import ThemeType from '../../themes/themeType'
import { matrix } from '@rn-matrix/expo'

export default function LoginScreen() {
  const [username, setUsername] = useState<string>('')
  const [password, setPassword] = useState<string>('')
  const [isLoading, setIsLoading] = useState<boolean>(false)

  const theme: ThemeType = useTheme()

  const passwordInput = useRef()

  const handleLoginPress = async () => {
    Keyboard.dismiss()
    setIsLoading(true)
    const response = await matrix.loginWithPassword(username, password, 'https://matrix.ditto.chat', false)
    if (response.error) {
      setIsLoading(false)
      // setErrorText(response.message)
    }
  }

  return (
    <Layout level="4" style={{ flex: 1, padding: Spacing.m }}>
      <SafeAreaView style={{ flex: 1, justifyContent: 'space-between' }}>
        <ScrollView keyboardShouldPersistTaps="handled">
          <View style={{ alignItems: 'center' }}>
            <Text style={{ fontSize: 32, marginVertical: Spacing.xl }}>scrollplay</Text>
            <Input
              placeholder="Username"
              textStyle={{ padding: Spacing.m }}
              autoCapitalize="none"
              autoCorrect={false}
              autoCompleteType="username"
              style={{ marginBottom: Spacing.s }}
              clearButtonMode="while-editing"
              onSubmitEditing={() => passwordInput.current.focus()}
              value={username}
              onChangeText={setUsername}
            />
            <Input
              ref={passwordInput}
              placeholder="Password"
              textStyle={{ padding: Spacing.m }}
              autoCapitalize="none"
              autoCorrect={false}
              autoCompleteType="password"
              secureTextEntry
              clearButtonMode="while-editing"
              value={password}
              onChangeText={setPassword}
            />
          </View>
        </ScrollView>

        <KeyboardAvoidingView behavior="padding">
          <Button
            size="large"
            onPress={handleLoginPress}
            disabled={username.length === 0 || password.length === 0}
            style={{ marginBottom: Spacing.xxl, height: 55 }}
            accessoryLeft={() => (isLoading ? <ActivityIndicator color={theme['text-basic-color']} /> : null)}
          >
            Login
          </Button>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </Layout>
  )
}
